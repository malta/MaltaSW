#!/bin/bash
export ITK_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export ITK_INST_PATH=${ITK_PATH}/installed

#setup TDAQ
export TDAQ_VERSION=tdaq-11-02-01

TDAQPATHS=(/home/atlas /cvmfs/atlas.cern.ch/repo/sw/tdaq)
for _cand in ${TDAQPATHS[@]}; do
    if [ -d ${_cand}/tdaq/${TDAQ_VERSION}/installed ]; then
	export TDAQ_RELEASE_BASE=${_cand}
	export CMAKE_PROJECT_PATH=${_cand}
	export TOOL_BASE=${_cand}/tools/x86_64-el9
        break
    fi
done
LCGPATHS=(/home/atlas/sw/lcg/releases /cvmfs/sft.cern.ch/lcg/releases)
for _cand in ${LCGPATHS[@]}; do
    if [ -d ${_cand} ]; then
	export LCG_RELEASE_BASE=${_cand}
	export LCG_INST_PATH=${_cand}
	break
    fi
done
if [ -z ${TDAQ_RELEASE_BASE+x} ]; then echo "TDAQ release base not found"; return 0; fi

echo "---------"
echo "TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE}"
echo "LCG_RELEASE_BASE=${LCG_RELEASE_BASE}"
echo "CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}"
source $TDAQ_RELEASE_BASE/tdaq/${TDAQ_VERSION}/installed/setup.sh 
source $TDAQC_INST_PATH/share/cmake_tdaq/bin/setup.sh 
echo "---------"

#setup ITK SW
export PATH=$ITK_INST_PATH/$CMTCONFIG/bin:$ITK_INST_PATH/share/bin:$PATH
export LD_LIBRARY_PATH=$ITK_INST_PATH/$CMTCONFIG/lib:$LD_LIBRARY_PATH

#setup Python
export PATH=$TDAQ_PYTHON_HOME/bin:$PATH
export PYTHONPATH=$ITK_INST_PATH/share/lib/python:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/$CMTCONFIG/lib:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/share/bin:$PYTHONPATH

#setup GDB
export PYTHONHOME=$(dirname $(dirname $(which python)))
export PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/13.2/${CMTCONFIG}/bin:$PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/13.2/${CMTCONFIG}/lib:$LD_LIBRARY_PATH

#setup Root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#missing libraries
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/glib/2.76.2/${CMTCONFIG}/lib64/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/pango/1.48.9/${CMTCONFIG}/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/harfbuzz/2.7.4/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/cairo/1.17.2/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/freetype/2.10.0/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/blas/0.3.20.openblas/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/uproot/4.3.7/${CMTCONFIG}/lib:$LD_LIBRARY_PATH

#doxygen
export PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/doxygen/1.8.18/${CMTCONFIG}/bin:$PATH

#Setup MALTA
export MALTA_INST_PATH=$ITK_INST_PATH
export MALTA_DATA_PATH=$ITK_PATH/data
export MALTA_LOGS_PATH=$ITK_PATH/logs
export MALTAPSU_SETUP=$ITK_PATH/MaltaPSUControl/

if [ -f "${MALTAPSU_SETUP}/share/setup-${HOSTNAME}.sh" ]; then source ${MALTAPSU_SETUP}/share/setup-${HOSTNAME}.sh; fi

echo " "
echo " HELLO! you are in "$HOSTNAME" .. so you will be using: "
echo "     - main folder    : "$MALTAPSU_SETUP
echo " "

alias COMPILE="cd ${ITK_PATH}/build; make install -j4; cd -;"
alias monitorPSU="MALTA_PSU.py -c monitorValues all None 1"

alias MINIMALTAON="MALTA_PSU.py -t miniMALTAON.txt"
alias MINIMALTAOFF="MALTA_PSU.py -t miniMALTAOFF.txt"

alias MINIMALTAON_2="MALTA_PSU.py -t miniMALTAON_2.txt"
alias MINIMALTAOFF_2="MALTA_PSU.py -t miniMALTAOFF_2.txt"

alias MALTAON="MALTA_PSU.py -t MALTAON.txt"
alias MALTAOFF="MALTA_PSU.py -t MALTAOFF.txt"

alias DUTON="MALTA_PSU.py -t DUTON.txt"
alias DUTOFF="MALTA_PSU.py -t DUTOFF.txt"

alias DUTONMALTA2="MALTA_PSU.py -t DUTON_MALTA2.txt"
alias DUTOFFMALTA2="MALTA_PSU.py -t DUTOFF_MALTA2.txt"

alias DUTONMIX="MALTA_PSU.py -t DUTON_MIX.txt"
alias DUTOFFMIX="MALTA_PSU.py -t DUTOFF_MIX.txt"

alias MALTA2DUTON="MALTA_PSU.py -t MALTA2DUTON.txt"
alias MALTA2DUTOFF="MALTA_PSU.py -t MALTA2DUTOFF.txt"

alias SPADs_DEFAULT="MALTA_PSU.py -t SPADs_default.txt"
alias SPADs_OFF="MALTA_PSU.py -t SPADS_OFF.txt"

export EOSNEW=/eos/atlas/atlascerngroupdisk/proj-cmos-sensors/
export EOSOLD=/eos/home-a/adecmos/
