# Malta Software
MALTA software is the collection of software to operate the MALTA family of Monolithic Pixel detectors (MALTA and Mini-MALTA), 
that is mostly written in C++, and includes Python tools and wrapper modules for the C++ libraries.
It is divided into several packages that provide different functionalities, based on the ATLAS TDAQ CMake project policy. 
TDAQ is required to compile and in some cases to run the software.

### Installation

1. Checkout MaltaSW package
```
git clone https://gitlab.cern.ch/malta/MaltaSW.git MaltaSW
```

2. Change to the MaltaSW directory
```
cd MaltaSW
```

3. Checkout the following packages:
```
git clone https://gitlab.cern.ch/malta/MaltaPSUControl.git MaltaPSUControl
git clone https://gitlab.cern.ch/malta/PySerialComm.git PySerialComm
git clone https://gitlab.cern.ch/malta/pyserial.git pyserial
git clone https://gitlab.cern.ch/malta/ipbus.git ipbus
git clone https://gitlab.cern.ch/malta/MaltaDAQ.git MaltaDAQ
git clone https://gitlab.cern.ch/malta/Malta.git Malta
git clone https://gitlab.cern.ch/malta/Malta2.git Malta2
git clone https://gitlab.cern.ch/malta/MiniMalta.git MiniMalta
git clone https://gitlab.cern.ch/malta/MiniMalta3.git MiniMalta3
git clone https://gitlab.cern.ch/malta/MaltaTLU.git MaltaTLU
```

4. Checkout additional packages (as you need):
```
git clone https://gitlab.cern.ch/malta/tkdiff tkdiff
git clone https://gitlab.cern.ch/malta/MaltaDiamond MaltaDiamond
git clone https://gitlab.cern.ch/malta/MaltaGlasgow MaltaGlasgow
git clone https://gitlab.cern.ch/solans/FpgaTools FpgaTools
```

5. Setup the environment
```
source setup.sh
```

6. Make a build directory
```
mkdir build
```

7. Change to the build directory
```
cd build
```

8. Config the release for building
```
cmake ..
```

9. Compile and install the software
```
make -j6 install
```

### Links

 * How to run the sofware is available in the following [link](@ref PackageMaltaDAQ)
 * ATLAS TDAQ is available in the following [Link](https://gitlab.cern.ch/atlas-tdaq-software)

